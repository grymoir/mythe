CREATE DATABASE "mythe";

-- Grant all privileges on the database to the postgres user
GRANT ALL PRIVILEGES ON DATABASE "mythe" TO "postgres";
