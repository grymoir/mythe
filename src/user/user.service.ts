import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async create(createUserInput: CreateUserInput) {
    const newUser = this.userRepository.create(createUserInput);
    try {
      const { generatedMaps } = await this.userRepository.insert(newUser);
      return { ...createUserInput, ...generatedMaps[0] };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async findAll() {
    return this.userRepository.find();
  }

  async findOne(id: string) {
    return this.userRepository.findOne({ where: { id } });
  }

  async update(id: number, updateUserInput: UpdateUserInput) {
    return `This action updates a #${id} user with ${updateUserInput}`;
  }

  async remove(id: string) {
    const removedUser = await this.userRepository.exists({ where: { id } });
    if (!removedUser) {
      throw new InternalServerErrorException('User not found');
    }
    return this.userRepository.delete(id);
  }
}
