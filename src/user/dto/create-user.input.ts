import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, Length, MinLength } from 'class-validator';
import { User } from '../entities/user.entity';
import { isUnique } from '../../utils/validators';

@InputType()
export class CreateUserInput implements Partial<User> {
  @Field(() => String, { nullable: false })
  @IsEmail()
  @isUnique({ tableName: 'user', column: 'email' })
  @MinLength(5)
  email: string;

  @Field(() => String, { nullable: false })
  @Length(2, 55)
  firstName: string;

  @Field(() => String, { nullable: false })
  @Length(2, 55)
  lastName: string;
}
