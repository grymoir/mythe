import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { ApolloServerPluginLandingPageLocalDefault } from '@apollo/server/plugin/landingPage/default';
import { Request, Response } from 'express';
import { DateScalar } from './scalars/date.scalar';
import { GraphQLError, GraphQLFormattedError } from 'graphql/error';

export type GraphqlContext = {
  req: Request;
  res: Response;
};

@Module({
  providers: [DateScalar],
  imports: [
    GraphQLModule.forRootAsync<ApolloDriverConfig>({
      driver: ApolloDriver,
      imports: [],
      inject: [],
      useFactory: () => {
        return {
          // @ts-expect-error ignore implicit any type
          context: ({ req, res }): GraphqlContext => ({
            req,
            res,
          }),
          formatError: (error: GraphQLError) => {
            const graphQLFormattedError: GraphQLFormattedError = {
              message:
                (error.extensions?.originalError as Error)?.message ||
                error.message,
            };
            return graphQLFormattedError;
          },
          autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
          sortSchema: true,
          path: '/graphql',
          playground: false,
          introspection: process.env.NAMESPACE !== 'production',
          plugins: [ApolloServerPluginLandingPageLocalDefault()],
          persistedQueries: false,
          fieldResolverEnhancers: ['interceptors', 'guards'],
        };
      },
    }),
  ],
})
export class GraphqlModule {}
